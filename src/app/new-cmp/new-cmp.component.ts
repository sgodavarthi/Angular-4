import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-new-cmp',
  templateUrl: './new-cmp.component.html',
  styleUrls: ['./new-cmp.component.css']
})
export class NewCmpComponent {
  title = 'Task';
  @Input('statusReceived')
  status: String;
  @Output()
  change: EventEmitter<String> = new EventEmitter<String>();

  display($event) {
   if ($event.target.value === '1') {
   this.status = 'All';
   } else if ($event.target.value === '2') {
      this.status = 'Expired';
    } else if ($event.target.value === '3') {
      this.status = 'Active';
    } else if ($event.target.value === '4') {
      this.status = 'Featured';
    }
    this.change.emit(this.status);
  }
}

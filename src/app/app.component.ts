import {Component} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  httpdata;
  status = 'All';
  constructor(private http: Http) {

    this.http.get('https://api.myjson.com/bins/ldv3x').map(
      (response) => response.json()
    ).subscribe(
      (data) => {
        this.displaydata(data);
      }
    );
  }

  updateData(itemsStatus: string) {
      this.status = itemsStatus;

  }

  displaydata(data) {
    this.httpdata = data;
  }
}
